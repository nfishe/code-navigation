package main

import "fmt"

func main() {
    saySomething("hello")
}

func saySomething(something string) {
    fmt.Println(something)
}
